---
lang: en
company: Chakra Evolution Yoga
jobTitle: Web Developer
workplace: Remote
website: https://chakra-evolution.com/
dateStart: 2018-05-01
dateEnd: 2018-10-31
---

I was responsible for the creation of the [chakra-evolution.com](https://chakra-evolution.com) website. It is powered by WordPress with the WooCoommerce plugin. I build the Chakra Evolution template from scratch to fit the ideas I had and to learn about the template creation in WordPress.

I was able to take an in-depth look into email server configuration and administration, web server setup, maintenance, support and overall technical administration and learned a lot in the process.
